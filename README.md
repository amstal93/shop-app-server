# Shop App Server [2019]

Build a Shop App Server.

## Used Frameworks / Libraries
- Docker [3.4]
- Linux Debian [Buster]
- PostgreSQL [11]
- Flask

## Usages
- used in [Shop App](https://gitlab.com/f1377/shap-app-flutter) for:
    - login
    - handling GET | POST | PUT | DELETE Request

## Server Information

### Start Server
- in the **docker** folder
`docker-compose up --build flask_deb9`

### End Server
- in the **docker** folder
`docker.compose down`

## Database

### Diagram
![DB Diagram](/images/readme/db_diagram.png "DB Diagram")
